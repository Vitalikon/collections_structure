﻿using System;
using Project_Structure.Common.Entities;
using Project_Structure.Common.Enums;

namespace Project_Structure.DAL.Repository
{
    public class UnitOfWork
    {
        private Repository<Project> _projectRepository;
        private Repository<Task> _taskRepository;
        private Repository<Team> _teamRepository;
        private Repository<User> _userRepository;


        public UnitOfWork()
        {
            CreateFakeData();
        }

        public Repository<Project> Projects => _projectRepository ??= new Repository<Project>();
        public Repository<Task> Tasks => _taskRepository ??= new Repository<Task>();
        public Repository<User> Users => _userRepository ??= new Repository<User>();
        public Repository<Team> Teams => _teamRepository ??= new Repository<Team>();


        //Fake Data for testing
        private void CreateFakeData()
        {
            CreateFakeProjects();
            CreateFakeUsers();
            CreateFakeTeams();
            CreateFakeTasks();
        }

        private void CreateFakeTasks()
        {
            _taskRepository = new Repository<Task>();
            _taskRepository.Insert(new Task
            {
                Description = "Nice description", Name = "Cool Task", State = TaskState.InProgress,
                CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 0, ProjectId = 0
            });
            _taskRepository.Insert(new Task
            {
                Description = "Nice description2", Name = "Cool Task2", State = TaskState.Done,
                CreatedAt = DateTime.Now, FinishedAt = DateTime.Now, PerformerId = 2, ProjectId = 1
            });
            _taskRepository.Insert(new Task
            {
                Description = "Nice description3", Name = "Cool Task", State = TaskState.InProgress,
                CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 3, ProjectId = 1
            });
            _taskRepository.Insert(new Task
            {
                Description = "Nice description4", Name = "Cool Task", State = TaskState.InProgress,
                CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 4, ProjectId = 2
            });
            _taskRepository.Insert(new Task
            {
                Description = "qwerty qwerty qwerty qwerty qwerty qwerty", Name = "qwerty",
                State = TaskState.Canceled,
                CreatedAt = DateTime.Now, FinishedAt = null, PerformerId = 0, ProjectId = 3
            });
        }

        private void CreateFakeTeams()
        {
            _teamRepository = new Repository<Team>();
            _teamRepository.Insert(new Team
            {
                Name = "Warriors", CreatedAt = DateTime.Now
            });
            _teamRepository.Insert(new Team
            {
                Name = "Victory", CreatedAt = DateTime.Now
            });
            _teamRepository.Insert(new Team
            {
                Name = "BSA", CreatedAt = DateTime.Now
            });
            _teamRepository.Insert(new Team
            {
                Name = "Ukraine", CreatedAt = DateTime.Now
            });
            _teamRepository.Insert(new Team
            {
                Name = "Ass", CreatedAt = DateTime.Now
            });
        }

        private void CreateFakeUsers()
        {
            _userRepository = new Repository<User>();
            _userRepository.Insert(new User
            {
                Email = "vitaliy.shatski@gmail.com", BirthDay = DateTime.Parse("2000-10-18"), FirstName = "Vitaliy",
                LastName = "Shatskiy", RegisteredAt = DateTime.Now, TeamId = 0
            });
            _userRepository.Insert(new User
            {
                Email = "sadszdf@gmail.com", BirthDay = DateTime.Now, FirstName = "DSFsdfsd",
                LastName = "FSsdfgwert", RegisteredAt = DateTime.Now, TeamId = 1
            });
            _userRepository.Insert(new User
            {
                Email = "33242dsax@gmail.com", BirthDay = DateTime.Now, FirstName = "cccxsdrf",
                LastName = "Fdsdcvd", RegisteredAt = DateTime.Now, TeamId = 0
            });
            _userRepository.Insert(new User
            {
                Email = "kkjkjh@gmail.com", BirthDay = DateTime.Now, FirstName = "Opdsfsd",
                LastName = "Ioekd", RegisteredAt = DateTime.Now, TeamId = 2
            });
            _userRepository.Insert(new User
            {
                Email = "dsdscdf@gmail.com", BirthDay = DateTime.Now, FirstName = "ytyty",
                LastName = "tytyttdfg", RegisteredAt = DateTime.Now, TeamId = 1
            });
        }

        private void CreateFakeProjects()
        {
            _projectRepository = new Repository<Project>();
            _projectRepository.Insert(new Project
            {
                Deadline = DateTime.Now, Description = "asd", Name = "sad", AuthorId = 0,
                CreatedAt = DateTime.Now, TeamId = 0
            });
            _projectRepository.Insert(new Project
            {
                Deadline = DateTime.Now, Description = "123213", Name = "asdf sadf", AuthorId = 1,
                CreatedAt = DateTime.Now, TeamId = 1
            });
            _projectRepository.Insert(new Project
            {
                Deadline = DateTime.Now, Description = "asasdfasgcfgfd", Name = "sasdc sdfad", AuthorId = 2,
                CreatedAt = DateTime.Now, TeamId = 2
            });
            _projectRepository.Insert(new Project
            {
                Deadline = DateTime.Now, Description = "aszcccccd", Name = "33332424sdf", AuthorId = 3,
                CreatedAt = DateTime.Now, TeamId = 3
            });
            _projectRepository.Insert(new Project
            {
                Deadline = DateTime.Now, Description = "bbbbb", Name = "222", AuthorId = 4,
                CreatedAt = DateTime.Now, TeamId = 4
            });
        }
    }
}