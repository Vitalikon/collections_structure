﻿using System.Collections.Generic;
using Project_Structure.Common.Entities.Abstract;

namespace Project_Structure.DAL.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        T Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}