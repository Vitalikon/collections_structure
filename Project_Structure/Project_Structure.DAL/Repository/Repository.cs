﻿using System;
using System.Collections.Generic;
using System.Linq;
using Project_Structure.Common.Entities.Abstract;

namespace Project_Structure.DAL.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly List<T> _entities = new();

        public IEnumerable<T> GetAll()
        {
            return _entities.AsEnumerable();
        }

        public T Get(int id)
        {
            return _entities.SingleOrDefault(s => s.Id == id);
        }

        public T Insert(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Id = _entities.Count;
            _entities.Add(entity);
            return entity;
        }


        public void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            var index = _entities.FindIndex(baseEntity => baseEntity.Id == entity.Id);
            if (index >= 0) _entities[index] = entity;
        }

        public void Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _entities.Remove(entity);
        }
    }
}