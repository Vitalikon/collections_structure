﻿using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Project_Structure.BLL.MappingProfiles;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Repository;

namespace Project_Structure.WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
                {
                    cfg.AddProfile<TeamProfile>();
                    cfg.AddProfile<TaskProfile>();
                    cfg.AddProfile<ProjectProfile>();
                    cfg.AddProfile<UserProfile>();
                },
                Assembly.GetExecutingAssembly());
        }


        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<TaskService>();
            services.AddScoped<ProjectService>();
            services.AddScoped<UserService>();
            services.AddScoped<TeamService>();
            services.AddSingleton<UnitOfWork>();
        }

        public static void RegisterSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Project_Structure.WebApi", Version = "v1"});
            });
        }

        public static void UseSwaggerWithUI(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Project_Structure.WebApi v1"));
        }
    }
}