﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Project_Structure.WebApi.Middlewares
{
    public sealed class GlobalExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public GlobalExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                response.StatusCode = GetResponseStatusCodeFromException(error);
                var result = JsonSerializer.Serialize(new
                {
                    title = "An server exception was thrown",
                    status = response.StatusCode,
                    errors = new Dictionary<string, string[]> {{error.GetType().Name, new[] {error.Message}}}
                });
                await response.WriteAsync(result);
            }
        }

        private static int GetResponseStatusCodeFromException(Exception error)
        {
            switch (error)
            {
                case FormatException:
                case InvalidOperationException:
                case ArgumentException:
                case BadHttpRequestException:
                    return (int) HttpStatusCode.BadRequest;
                case KeyNotFoundException:
                    return (int) HttpStatusCode.NotFound;
                default:
                    return (int) HttpStatusCode.InternalServerError;
            }
        }
    }
}