using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Services;
using Project_Structure.Common.DTO.Helper;
using Project_Structure.Common.DTO.Team;

namespace Project_Structure.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }

        // GET: api/Teams
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_teamService.GetAllTeams());
        }

        // GET: api/Teams/5
        [HttpGet("{id:int}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            return Ok(_teamService.GetTeamById(id));
        }

        // POST: api/Teams
        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] CreateTeamDTO team)
        {
            return Ok(_teamService.CreateTeam(team));
        }

        // PUT: api/Teams
        [HttpPut]
        public ActionResult<TeamDTO> Put([FromBody] UpdateTeamDTO team)
        {
            return Ok(_teamService.UpdateTeam(team));
        }

        // DELETE: api/Teams/5
        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            _teamService.DeleteTeam(id);
            return NoContent();
        }

        //4. GET: api/Teams/groupedUsers/5
        [HttpGet("groupedUsers")]
        public ActionResult<List<QueryDTO4>> GetListOfTeamsOlder10SortedGrouped()
        {
            return Ok(_teamService.GetListOfTeamsOlder10SortedGrouped());
        }
    }
}