using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Services;
using Project_Structure.Common.DTO.Helper;
using Project_Structure.Common.DTO.Task;
using Project_Structure.Common.DTO.User;

namespace Project_Structure.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_userService.GetAllUsers());
        }

        // GET: api/Users/5
        [HttpGet("{id:int}")]
        public ActionResult<UserDTO> GetById(int id)
        {
            return Ok(_userService.GetUserById(id));
        }

        // POST: api/Users
        [HttpPost]
        public ActionResult<UserDTO> Post([FromBody] CreateUserDTO userDTO)
        {
            return Ok(_userService.CreateUser(userDTO));
        }

        // PUT: api/Users
        [HttpPut]
        public ActionResult<UserDTO> Put([FromBody] UpdateUserDTO userDTO)
        {
            return Ok(_userService.UpdateUser(userDTO));
        }

        // DELETE: api/Users/5
        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            _userService.DeleteUser(id);
            return NoContent();
        }

        //5. GET: api/Users/withTasks
        [HttpGet("withTasks")]
        public ActionResult<List<UserTasksDTO>> GetUsersSortedByAscendingWithTasksSortedByDescending()
        {
            return Ok(_userService.GetUsersSortedByAscendingWithTasksSortedByDescending());
        }

        //6. GET: api/Users/5/getInfo
        [HttpGet("{id:int}/getInfo")]
        public ActionResult<UserInfoDTO> GetUserInfo(int id)
        {
            return Ok(_userService.GetUserInfoDTO(id));
        }
    }
}