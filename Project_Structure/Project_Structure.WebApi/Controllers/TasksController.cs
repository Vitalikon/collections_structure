using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Services;
using Project_Structure.Common.DTO.Helper;
using Project_Structure.Common.DTO.Project;
using Project_Structure.Common.DTO.Task;

namespace Project_Structure.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_taskService.GetAllTasks());
        }

        // GET: api/Tasks/5
        [HttpGet("{id:int}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult<TaskDTO> Post([FromBody] CreateTaskDTO dto)
        {
            return Ok(_taskService.CreateTask(dto));
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult<TaskDTO> Put([FromBody] UpdateTaskDTO dto)
        {
            return Ok(_taskService.UpdateTask(dto));
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }


        //1. GET: api/Tasks/countFromProjectsByAuthorId/5
        [HttpGet("countFromProjectsByAuthorId/{id:int}")]
        public ActionResult<Dictionary<ProjectDTO, int>> GetCountOfTasksFromProjectsByAuthorId(int id)
        {
            return Ok(_taskService.GetCountOfTasksFromProjectsByAuthorId(id).ToList());
        }

        //2. GET: api/Tasks/byPerformerId/5
        [HttpGet("byPerformerId/{id:int}")]
        public ActionResult<List<TaskDTO>> GetTasksByPerformerId(int id)
        {
            return Ok(_taskService.GetTasksByPerformerId(id));
        }

        //3. GET: api/Tasks/finishedByPerformerId/5
        [HttpGet("finishedByPerformerId/{id:int}")]
        public ActionResult<List<QueryDTO3>> GetListOfFinishedTasksByPerformerId(int id)
        {
            return Ok(_taskService.GetListOfFinishedTasksByPerformerId(id));
        }
    }
}