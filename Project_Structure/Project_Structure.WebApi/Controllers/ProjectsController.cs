using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Services;
using Project_Structure.Common.DTO.Project;

namespace Project_Structure.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetAllProjects());
        }

        // GET: api/Projects/5
        [HttpGet("{id:int}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }

        // POST: api/Projects
        [HttpPost]
        public ActionResult<ProjectDTO> Post([FromBody] CreateProjectDTO dto)
        {
            return Ok(_projectService.CreateProject(dto));
        }

        // PUT: api/Projects
        [HttpPut]
        public ActionResult<ProjectDTO> Put([FromBody] UpdateProjectDTO dto)
        {
            return Ok(_projectService.UpdateProject(dto));
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }

        //7. GET: api/Projects/getInfo
        [HttpGet("getInfo")]
        public ActionResult<ProjectInfoDTO> GetProjectsInfo()
        {
            return Ok(_projectService.GetProjectsInfoDTO());
        }
    }
}