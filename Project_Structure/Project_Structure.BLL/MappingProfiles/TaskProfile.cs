﻿using AutoMapper;
using Project_Structure.Common.DTO.Task;
using Project_Structure.Common.Entities;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDTO, Task>().ReverseMap();
            CreateMap<CreateTaskDTO, Task>();
            CreateMap<UpdateTaskDTO, Task>();
        }
    }
}