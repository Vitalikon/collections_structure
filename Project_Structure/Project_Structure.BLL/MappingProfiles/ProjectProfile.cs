﻿using AutoMapper;
using Project_Structure.Common.DTO.Project;
using Project_Structure.Common.Entities;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>().ReverseMap();
            CreateMap<CreateProjectDTO, Project>();
            CreateMap<UpdateProjectDTO, Project>();
        }
    }
}