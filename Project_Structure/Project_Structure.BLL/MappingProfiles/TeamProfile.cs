﻿using AutoMapper;
using Project_Structure.Common.DTO.Team;
using Project_Structure.Common.Entities;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>().ReverseMap();
            CreateMap<CreateTeamDTO, Team>();
            CreateMap<UpdateTeamDTO, Team>();
        }
    }
}