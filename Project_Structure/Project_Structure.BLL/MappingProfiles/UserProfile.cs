﻿using AutoMapper;
using Project_Structure.Common.DTO.User;
using Project_Structure.Common.Entities;

namespace Project_Structure.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDTO, User>().ReverseMap();
            CreateMap<CreateUserDTO, User>();
            CreateMap<UpdateUserDTO, User>();
        }
    }
}