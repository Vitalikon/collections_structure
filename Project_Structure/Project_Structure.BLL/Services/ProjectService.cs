﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.Common.DTO.Project;
using Project_Structure.Common.DTO.Task;
using Project_Structure.Common.Entities;
using Project_Structure.DAL.Repository;

namespace Project_Structure.BLL.Services
{
    public sealed class ProjectService : BaseService
    {
        public ProjectService(IMapper mapper, UnitOfWork context) : base(mapper, context)
        {
        }

        public ICollection<ProjectDTO> GetAllProjects()
        {
            var projects = _context.Projects.GetAll();
            if (!projects.Any())
                throw new KeyNotFoundException("Project not found");

            return _mapper.Map<ICollection<ProjectDTO>>(projects);
        }

        public ProjectDTO GetProjectById(int projectId)
        {
            var project = _context.Projects.Get(projectId);
            if (project == null)
                throw new KeyNotFoundException("Project not found");

            return _mapper.Map<ProjectDTO>(project);
        }

        public ProjectDTO CreateProject(CreateProjectDTO createProjectDTO)
        {
            if (_context.Users.Get(createProjectDTO.AuthorId) == null)
                throw new KeyNotFoundException("Author not found");

            if (_context.Teams.Get(createProjectDTO.TeamId) == null) throw new KeyNotFoundException("Team not found");

            var newProjectEntity = _mapper.Map<Project>(createProjectDTO);
            newProjectEntity.CreatedAt = DateTime.Now;
            var created = _context.Projects.Insert(newProjectEntity);
            return _mapper.Map<ProjectDTO>(created);
        }

        public ProjectDTO UpdateProject(UpdateProjectDTO updateProjectDTO)
        {
            var oldProject = _context.Projects.Get(updateProjectDTO.Id);
            if (oldProject == null) throw new KeyNotFoundException("Project not found");

            if (_context.Users.Get(updateProjectDTO.AuthorId) == null)
                throw new KeyNotFoundException("Author not found");

            if (_context.Teams.Get(updateProjectDTO.TeamId) == null) throw new KeyNotFoundException("Team not found");

            if (!string.IsNullOrWhiteSpace(updateProjectDTO.Name)) oldProject.Name = updateProjectDTO.Name;

            if (!string.IsNullOrWhiteSpace(updateProjectDTO.Description))
                oldProject.Description = updateProjectDTO.Description;

            oldProject.Deadline = updateProjectDTO.Deadline;
            oldProject.AuthorId = updateProjectDTO.AuthorId;
            oldProject.TeamId = updateProjectDTO.TeamId;

            _context.Projects.Update(oldProject);
            return _mapper.Map<ProjectDTO>(oldProject);
        }

        public void DeleteProject(int projectId)
        {
            var projectToDelete = _context.Projects.Get(projectId);
            if (projectToDelete == null) throw new KeyNotFoundException("Project not found");

            _context.Projects.Delete(projectToDelete);
        }


        // #7 Получить ProjectInfo
        // Самый длинный таск проекта (по описанию)
        // Самый короткий таск проекта (по имени)
        // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3
        public List<ProjectInfoDTO> GetProjectsInfoDTO()
        {
            return _context.Projects.GetAll()
                .GroupJoin(_context.Users.GetAll(),
                    project => project.TeamId,
                    user => user.TeamId,
                    (project, usersInTeam) =>
                    {
                        var usersInTeamCount = 0;
                        var projectTasks = _context.Tasks.GetAll().Where(task => task.Id == project.Id).ToList();
                        if (project.Description.Length > 20 || projectTasks.Count < 3)
                            usersInTeamCount = usersInTeam.Count();
                        return new {project, usersCount = usersInTeamCount, projectTasks};
                    })
                .Select(arg =>
                    new ProjectInfoDTO(
                        _mapper.Map<TaskDTO>(arg.projectTasks
                            .OrderByDescending(task => task.Description.Length)
                            .FirstOrDefault()),
                        _mapper.Map<TaskDTO>(arg.projectTasks
                            .OrderBy(task => task.Name.Length)
                            .FirstOrDefault()),
                        arg.usersCount,
                        _mapper.Map<ProjectDTO>(arg.project))
                )
                .ToList();
        }
    }
}