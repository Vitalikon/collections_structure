﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.Common.DTO.Helper;
using Project_Structure.Common.DTO.Project;
using Project_Structure.Common.DTO.Task;
using Project_Structure.Common.DTO.User;
using Project_Structure.Common.Entities;
using Project_Structure.Common.Enums;
using Project_Structure.DAL.Repository;

namespace Project_Structure.BLL.Services
{
    public sealed class UserService : BaseService
    {
        public UserService(IMapper mapper, UnitOfWork context) : base(mapper, context)
        {
        }

        public ICollection<UserDTO> GetAllUsers()
        {
            var users = _context.Users.GetAll();
            if (!users.Any()) throw new KeyNotFoundException("Users not found");

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public UserDTO GetUserById(int projectId)
        {
            var user = _context.Users.Get(projectId);
            if (user == null) throw new KeyNotFoundException("User not found");

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO CreateUser(CreateUserDTO createUserDTO)
        {
            if (createUserDTO.TeamId != null && _context.Teams.Get(createUserDTO.TeamId.Value) == null)
                throw new KeyNotFoundException("Team not found");

            var newUserEntity = _mapper.Map<User>(createUserDTO);
            newUserEntity.RegisteredAt = DateTime.Now;
            var created = _context.Users.Insert(newUserEntity);
            return _mapper.Map<UserDTO>(created);
        }

        public UserDTO UpdateUser(UpdateUserDTO updateUserDTO)
        {
            var oldUser = _context.Users.Get(updateUserDTO.Id);
            if (oldUser == null) throw new KeyNotFoundException("User not found");

            if (updateUserDTO.TeamId != null && _context.Teams.Get(updateUserDTO.TeamId.Value) == null)
                throw new KeyNotFoundException("Team not found");

            if (!string.IsNullOrWhiteSpace(updateUserDTO.Email)) oldUser.Email = updateUserDTO.Email;

            if (!string.IsNullOrWhiteSpace(updateUserDTO.FirstName)) oldUser.FirstName = updateUserDTO.FirstName;

            if (!string.IsNullOrWhiteSpace(updateUserDTO.LastName)) oldUser.LastName = updateUserDTO.LastName;

            if (updateUserDTO.TeamId != null)
                oldUser.TeamId = updateUserDTO.TeamId.Value;
            oldUser.BirthDay = updateUserDTO.BirthDay;

            _context.Users.Update(oldUser);
            return _mapper.Map<UserDTO>(oldUser);
        }

        public void DeleteUser(int userId)
        {
            var userToDelete = _context.Users.Get(userId);
            if (userToDelete == null) throw new KeyNotFoundException("User not found");

            _context.Users.Delete(userToDelete);
        }

        //5. Получить список пользователей по алфавиту first_name (по возрастанию)
        //с отсортированными tasks по длине name (по убыванию).
        public List<UserTasksDTO> GetUsersSortedByAscendingWithTasksSortedByDescending()
        {
            return _context.Users.GetAll()
                .OrderBy(user => user.FirstName)
                .GroupJoin(_context.Tasks.GetAll(),
                    user => user.Id,
                    task => task.PerformerId,
                    (user, tasks) => new UserTasksDTO
                    {
                        User = _mapper.Map<UserDTO>(user),
                        Tasks = _mapper.Map<List<TaskDTO>>(tasks.OrderByDescending(task => task.Name.Length))
                    })
                .ToList();
        }

        //6. Получить UserInfo (передать Id пользователя в параметры)
        // Последний проект пользователя (по дате создания)
        // Общее кол-во тасков под последним проектом
        // Общее кол-во незавершенных или отмененных тасков для пользователя
        // Самый долгий таск пользователя по дате (раньше всего создан - позже всего закончен)(фильтр по дате)
        public UserInfoDTO GetUserInfoDTO(int userId)
        {
            return _context.Users.GetAll()
                .Where(user => user.Id == userId)
                .GroupJoin(_context.Projects.GetAll(),
                    user => user.Id,
                    project => project.AuthorId,
                    (user, projects) => new {User = user, Projects = projects})
                .GroupJoin(_context.Tasks.GetAll(),
                    userWithProjects => userWithProjects.User.Id,
                    task => task.PerformerId,
                    (userWithProjects, tasks) => //+ user tasks
                    {
                        var projectWithTasksCount = userWithProjects.Projects
                            .OrderByDescending(project => project.CreatedAt)
                            .Select(project => new
                                {project, Count = tasks.Count(task => task.ProjectId == project.Id)})
                            .FirstOrDefault();
                        tasks = tasks.ToList();
                        return new UserInfoDTO(_mapper.Map<ProjectDTO>(projectWithTasksCount?.project),
                            projectWithTasksCount?.Count ?? 0,
                            tasks.Count(task => task.FinishedAt == null || task.State == TaskState.Canceled),
                            _mapper.Map<TaskDTO>(tasks.Where(task => task.FinishedAt != null)
                                .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                                .FirstOrDefault()),
                            _mapper.Map<UserDTO>(userWithProjects.User)
                        );
                    }
                ).FirstOrDefault();
        }
    }
}