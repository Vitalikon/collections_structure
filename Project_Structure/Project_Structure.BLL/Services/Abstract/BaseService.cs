﻿using AutoMapper;
using Project_Structure.DAL.Repository;

namespace Project_Structure.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly UnitOfWork _context;
        private protected readonly IMapper _mapper;

        public BaseService(IMapper mapper, UnitOfWork context)
        {
            _mapper = mapper;
            _context = context;
        }
    }
}