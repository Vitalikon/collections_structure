﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.Common.DTO.Helper;
using Project_Structure.Common.DTO.Project;
using Project_Structure.Common.DTO.Task;
using Project_Structure.Common.Entities;
using Project_Structure.DAL.Repository;

namespace Project_Structure.BLL.Services
{
    public sealed class TaskService : BaseService
    {
        public TaskService(IMapper mapper, UnitOfWork context) : base(mapper, context)
        {
        }

        public ICollection<TaskDTO> GetAllTasks()
        {
            var tasks = _context.Tasks.GetAll();
            if (!tasks.Any())
                throw new KeyNotFoundException("Tasks not found");

            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        public TaskDTO GetTaskById(int taskId)
        {
            var task = _context.Tasks.Get(taskId);
            if (task == null)
                throw new KeyNotFoundException("Task not found");

            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO CreateTask(CreateTaskDTO createTaskDTO)
        {
            if (_context.Users.Get(createTaskDTO.PerformerId) == null)
                throw new KeyNotFoundException("Performer not found");

            if (_context.Projects.Get(createTaskDTO.ProjectId) == null)
                throw new KeyNotFoundException("Project not found");

            var newTaskEntity = _mapper.Map<Task>(createTaskDTO);
            newTaskEntity.CreatedAt = DateTime.Now;
            newTaskEntity.FinishedAt = null;
            var created = _context.Tasks.Insert(newTaskEntity);
            return _mapper.Map<TaskDTO>(created);
        }

        public TaskDTO UpdateTask(UpdateTaskDTO updateTaskDTO)
        {
            var oldTask = _context.Tasks.Get(updateTaskDTO.Id);
            if (oldTask == null) throw new KeyNotFoundException("Task not found");

            if (_context.Users.Get(updateTaskDTO.PerformerId) == null)
                throw new KeyNotFoundException("Performer not found");

            if (_context.Projects.Get(updateTaskDTO.ProjectId) == null)
                throw new KeyNotFoundException("Project not found");

            if (!string.IsNullOrWhiteSpace(updateTaskDTO.Name)) oldTask.Name = updateTaskDTO.Name;

            if (!string.IsNullOrWhiteSpace(updateTaskDTO.Description)) oldTask.Description = updateTaskDTO.Description;

            if (updateTaskDTO.FinishedAt != null) oldTask.FinishedAt = updateTaskDTO.FinishedAt;

            oldTask.PerformerId = updateTaskDTO.PerformerId;
            oldTask.ProjectId = updateTaskDTO.ProjectId;
            oldTask.State = updateTaskDTO.State;

            _context.Tasks.Update(oldTask);
            return _mapper.Map<TaskDTO>(oldTask);
        }

        public void DeleteTask(int taskId)
        {
            var taskToDelete = _context.Tasks.Get(taskId);
            if (taskToDelete == null) throw new KeyNotFoundException("Task not found");

            _context.Tasks.Delete(taskToDelete);
        }

        //1. Получить кол-во тасков у проекта конкретного пользователя (по id)
        //(словарь, где ключом будет проект, а значением кол-во тасков).
        public Dictionary<ProjectDTO, int> GetCountOfTasksFromProjectsByAuthorId(int userId)
        {
            var tasks = _context.Tasks.GetAll();
            var result = _context.Projects.GetAll()
                .Where(project => project.AuthorId == userId)
                .ToDictionary(project => _mapper.Map<ProjectDTO>(project), project =>
                    tasks.Count(task => task.ProjectId == project.Id));
            return result;
        }

        //2. Получить список тасков, назначенных на конкретного пользователя (по id),
        //где name таска < 45 символов (коллекция из тасков).
        public List<TaskDTO> GetTasksByPerformerId(int userId)
        {
            return _mapper.Map<List<TaskDTO>>(
                _context.Tasks
                    .GetAll()
                    .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                    .ToList());
        }

        //3. Получить список (id, name) из коллекции тасков,
        // которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id).
        public List<QueryDTO3> GetListOfFinishedTasksByPerformerId(int userId)
        {
            return _context.Tasks
                .GetAll()
                .Where(task => task.FinishedAt is {Year: 2021} && task.ProjectId == userId)
                .Select(task => new QueryDTO3 {Id = task.Id, Name = task.Name})
                .ToList();
        }
    }
}