﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.Common.DTO.Helper;
using Project_Structure.Common.DTO.Team;
using Project_Structure.Common.DTO.User;
using Project_Structure.Common.Entities;
using Project_Structure.DAL.Repository;

namespace Project_Structure.BLL.Services
{
    public sealed class TeamService : BaseService
    {
        public TeamService(IMapper mapper, UnitOfWork context) : base(mapper, context)
        {
        }

        public ICollection<TeamDTO> GetAllTeams()
        {
            var team = _context.Teams.GetAll();
            if (!team.Any()) throw new KeyNotFoundException("Team not found");

            return _mapper.Map<ICollection<TeamDTO>>(team);
        }

        public TeamDTO GetTeamById(int teamId)
        {
            var team = _context.Teams.Get(teamId);
            if (team == null) throw new KeyNotFoundException("Team not found");

            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO CreateTeam(CreateTeamDTO createTeamDTO)
        {
            var newTeamEntity = _mapper.Map<Team>(createTeamDTO);
            newTeamEntity.CreatedAt = DateTime.Now;
            var created = _context.Teams.Insert(newTeamEntity);
            return _mapper.Map<TeamDTO>(created);
        }

        public TeamDTO UpdateTeam(UpdateTeamDTO updateTeamDTO)
        {
            var oldTeam = _context.Teams.Get(updateTeamDTO.Id);
            if (oldTeam == null) throw new KeyNotFoundException("Team not found");

            if (!string.IsNullOrWhiteSpace(updateTeamDTO.Name)) oldTeam.Name = updateTeamDTO.Name;

            _context.Teams.Update(oldTeam);
            return _mapper.Map<TeamDTO>(oldTeam);
        }

        public void DeleteTeam(int teamId)
        {
            var teamToDelete = _context.Teams.Get(teamId);
            if (teamToDelete == null) throw new KeyNotFoundException("Team not found");

            _context.Teams.Delete(teamToDelete);
        }


        //4. Получить список (id, имя команды и список пользователей)
        //из коллекции команд, участники которых старше 10 лет,
        //отсортированных по дате регистрации пользователя по убыванию,
        //а также сгруппированных по командам.
        public List<QueryDTO4> GetListOfTeamsOlder10SortedGrouped()
        {
            return _context.Teams
                .GetAll()
                .GroupJoin(_context.Users
                        .GetAll()
                        .Where(user => user.BirthDay.Year < DateTime.Now.Year - 10)
                        .OrderByDescending(user => user.RegisteredAt),
                    team => team.Id,
                    user => user.TeamId,
                    (team, users) => new QueryDTO4
                        {Id = team.Id, Name = team.Name, Users = _mapper.Map<List<UserDTO>>(users.ToList())})
                .Where(dto4 => dto4.Users.Any())
                .ToList();
        }
    }
}