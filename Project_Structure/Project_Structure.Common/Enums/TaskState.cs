﻿namespace Project_Structure.Common.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}