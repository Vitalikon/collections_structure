﻿using System;

namespace Project_Structure.Common.DTO.Project
{
    public sealed class CreateProjectDTO
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}