﻿using System;

namespace Project_Structure.Common.DTO.Team
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}