﻿namespace Project_Structure.Common.DTO.Team
{
    public sealed class UpdateTeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}