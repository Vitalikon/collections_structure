﻿namespace Project_Structure.Common.DTO.Team
{
    public sealed class CreateTeamDTO
    {
        public string Name { get; set; }
    }
}