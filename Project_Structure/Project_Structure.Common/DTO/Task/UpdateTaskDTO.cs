﻿using System;
using Project_Structure.Common.Enums;

namespace Project_Structure.Common.DTO.Task
{
    public sealed class UpdateTaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}