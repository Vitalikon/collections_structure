﻿using System.Collections.Generic;
using Project_Structure.Common.DTO.User;

namespace Project_Structure.Common.DTO.Helper
{
    public sealed class QueryDTO4
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}