﻿using System.Collections.Generic;
using Project_Structure.Common.DTO.Task;
using Project_Structure.Common.DTO.User;

namespace Project_Structure.Common.DTO.Helper
{
    public class UserTasksDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}