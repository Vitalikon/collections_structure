﻿using Project_Structure.Common.DTO.Project;
using Project_Structure.Common.DTO.Task;

namespace Project_Structure.Common.DTO.User
{
    public class UserInfoDTO
    {
        // User
        // Последний проект пользователя (по дате создания)
        // Общее кол-во тасков под последним проектом
        // Общее кол-во незавершенных или отмененных тасков для пользователя
        // Самый долгий таск пользователя по дате (раньше всего создан - позже всего закончен)(фильтр по дате)

        public UserInfoDTO(
            ProjectDTO lastProject,
            int lastProjectTasksCount,
            int countOfNotFinishedTasks,
            TaskDTO longestCompletedTaskByTime,
            UserDTO user)
        {
            LastProject = lastProject;
            LastProjectTasksCount = lastProjectTasksCount;
            CountOfNotFinishedTasks = countOfNotFinishedTasks;
            LongestCompletedTaskByTime = longestCompletedTaskByTime;
            User = user;
        }

        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int CountOfNotFinishedTasks { get; set; }
        public TaskDTO LongestCompletedTaskByTime { get; set; }
    }
}