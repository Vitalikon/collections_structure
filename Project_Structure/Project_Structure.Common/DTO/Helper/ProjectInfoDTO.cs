﻿using Project_Structure.Common.DTO.Task;

namespace Project_Structure.Common.DTO.Project
{
    //7. Проект
    // Самый длинный таск проекта (по описанию)
    // Самый короткий таск проекта (по имени)
    // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3
    public class ProjectInfoDTO
    {
        public ProjectInfoDTO(
            TaskDTO longestTaskByDescription,
            TaskDTO shortestTaskByName,
            int countOfUsersInTeamsByCondition,
            ProjectDTO project)
        {
            LongestTaskByDescription = longestTaskByDescription;
            ShortestTaskByName = shortestTaskByName;
            CountOfUsersInTeamsByCondition = countOfUsersInTeamsByCondition;
            Project = project;
        }

        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int CountOfUsersInTeamsByCondition { get; set; }
        public ProjectDTO Project { get; set; }
    }
}