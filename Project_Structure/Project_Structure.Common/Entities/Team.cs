﻿using System;
using Project_Structure.Common.Entities.Abstract;

namespace Project_Structure.Common.Entities
{
    public sealed class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}