﻿using System;
using Project_Structure.Common.Entities.Abstract;

namespace Project_Structure.Common.Entities
{
    public sealed class Project : BaseEntity
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}