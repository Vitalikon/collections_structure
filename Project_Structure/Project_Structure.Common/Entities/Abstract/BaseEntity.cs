﻿namespace Project_Structure.Common.Entities.Abstract
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}