﻿using System;
using Project_Structure.Common.Entities.Abstract;
using Project_Structure.Common.Enums;

namespace Project_Structure.Common.Entities
{
    public sealed class Task : BaseEntity
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}