﻿using System;
using System.Net;
using System.Net.Http;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Newtonsoft.Json;


namespace Collections_LINQ.ConsoleApplication.Helpers
{
    public static class ExceptionThrowers
    {
        public static void ThrowExceptionFromResponseBody(string content)
        {
            var deserializedException = JsonConvert.DeserializeObject<HttpExceptionModel>(content);
            if (deserializedException == null)
                throw new ArgumentNullException("deserializedException", "Exception deserialization failed!");

            throw new HttpRequestException(deserializedException.ToString(), null,
                (HttpStatusCode) deserializedException.Status);
        }
    }
}