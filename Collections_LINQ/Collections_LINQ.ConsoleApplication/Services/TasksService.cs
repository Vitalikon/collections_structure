﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Collections_LINQ.ConsoleApplication.Services.Abstract;
using SystemThreadingTask = System.Threading.Tasks.Task;
using Task = Collections_LINQ.ConsoleApplication.Models.Entities.Task;


namespace Collections_LINQ.ConsoleApplication.Services
{
    public class TasksService : BaseService<Task>
    {
        public TasksService(DataStore dataStore, IProvider<Task> provider) : base(dataStore, provider)
        {
        }


        //1. Получить кол-во тасков у проекта конкретного пользователя (по id)
        //(словарь, где ключом будет проект, а значением кол-во тасков).
        public Dictionary<Project, int> GetCountOfTasksFromProjectsByAuthorId(int userId)
        {
            return _dataStore.Projects
                .Where(project => project.AuthorId == userId)
                .ToDictionary(project => project, project => project.Tasks.Count);
        }

        //2. Получить список тасков, назначенных на конкретного пользователя (по id),
        //где name таска < 45 символов (коллекция из тасков).
        public List<Task> GetTasksByPerformerId(int userId)
        {
            return _dataStore.Projects.SelectMany(project => project.Tasks)
                .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                .ToList();
        }

        //3. Получить список (id, name) из коллекции тасков,
        // которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id).
        public List<(int Id, string Name)> GetListOfFinishedTasksByPerformerId(int userId)
        {
            return _dataStore.Projects
                .SelectMany(project => project.Tasks)
                .Where(task => task.FinishedAt is {Year: 2021} && task.PerformerId == userId)
                .Select(task => (task.Id, task.Name))
                .ToList();
        }


        public async Task<Dictionary<Project, int>> GetCountOfTasksFromProjectsByAuthorIdWebApi(int userId)
        {
            return await ((TasksProvider) _provider).GetCountFromProjectsByAuthorId(userId);
        }

        public async Task<List<Task>> GetTasksByPerformerIdWebApi(int userId)
        {
            return await ((TasksProvider) _provider).GetTasksByPerformerId(userId);
        }

        public async Task<List<Query3>> GetListOfFinishedTasksByPerformerIdWebApi(int userId)
        {
            return await ((TasksProvider) _provider).GetTasksFinishedByPerformerId(userId);
        }

        public async Task<List<Task>> Get()
        {
            return await ((TasksProvider) _provider).Get();
        }

        public async Task<Task> GetById(int id)
        {
            return await ((TasksProvider) _provider).GetById(id);
        }

        public async Task<Task> Create(Task task)
        {
            return await ((TasksProvider) _provider).Post(task);
        }

        public async Task<Task> Update(Task task)
        {
            return await ((TasksProvider) _provider).Put(task);
        }

        public async void Delete(int id)
        {
            await ((TasksProvider) _provider).Delete(id);
        }
    }
}