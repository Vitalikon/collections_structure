﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Collections_LINQ.ConsoleApplication.Services.Abstract;

namespace Collections_LINQ.ConsoleApplication.Services
{
    public class ProjectsService : BaseService<Project>
    {
        public ProjectsService(DataStore dataStore, IProvider<Project> provider) : base(dataStore, provider)
        {
        }

        public async Task<List<Project>> Get()
        {
            return await ((ProjectsProvider) _provider).Get();
        }

        public async Task<Project> GetById(int id)
        {
            return await ((ProjectsProvider) _provider).GetById(id);
        }

        public async Task<Project> Create(Project project)
        {
            return await ((ProjectsProvider) _provider).Post(project);
        }

        public async Task<Project> Update(Project project)
        {
            return await ((ProjectsProvider) _provider).Put(project);
        }

        public async void Delete(int id)
        {
            await ((ProjectsProvider) _provider).Delete(id);
        }

        // #7 Получить ProjectInfo
        // Самый длинный таск проекта (по описанию)
        // Самый короткий таск проекта (по имени)
        // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3
        public List<ProjectInfo> GetProjectsInfo()
        {
            return _dataStore.Projects
                .GroupJoin(_dataStore.Projects
                        .Select(project => project.Author)
                        .Union(_dataStore.Projects
                            .SelectMany(project => project.Tasks)
                            .Select(task => task.Performer))
                        .Distinct(),
                    project => project.TeamId,
                    user => user.TeamId,
                    (project, usersInTeam) =>
                    {
                        var usersInTeamCount = 0;
                        if (project.Description.Length > 20 || project.Tasks.Count < 3)
                            usersInTeamCount = usersInTeam.Count();

                        return new {project, usersCount = usersInTeamCount};
                    }
                )
                .Select(arg =>
                {
                    return new ProjectInfo(
                        arg.project.Tasks
                            .OrderByDescending(task => task.Description.Length)
                            .FirstOrDefault(),
                        arg.project.Tasks
                            .OrderBy(task => task.Name.Length)
                            .FirstOrDefault(),
                        arg.usersCount,
                        arg.project);
                })
                .ToList();
        }

        public async Task<List<ProjectInfo>> GetProjectsInfoWebApi()
        {
            return await ((ProjectsProvider) _provider).GetProjectsInfo();
        }
    }
}