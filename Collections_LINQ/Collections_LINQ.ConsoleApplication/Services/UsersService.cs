﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Enums;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Collections_LINQ.ConsoleApplication.Services.Abstract;
using Task = Collections_LINQ.ConsoleApplication.Models.Entities.Task;

namespace Collections_LINQ.ConsoleApplication.Services
{
    public class UsersService : BaseService<User>
    {
        public UsersService(DataStore dataStore, IProvider<User> provider) : base(dataStore, provider)
        {
        }

        public async Task<List<User>> Get()
        {
            return await ((UsersProvider) _provider).Get();
        }

        public async Task<User> GetById(int id)
        {
            return await ((UsersProvider) _provider).GetById(id);
        }

        public async Task<User> Create(User user)
        {
            return await ((UsersProvider) _provider).Post(user);
        }

        public async Task<User> Update(User user)
        {
            return await ((UsersProvider) _provider).Put(user);
        }

        public async void Delete(int id)
        {
            await ((UsersProvider) _provider).Delete(id);
        }


        //5. Получить список пользователей по алфавиту first_name (по возрастанию)
        //с отсортированными tasks по длине name (по убыванию).
        public Dictionary<User, List<Task>> GetUsersSortedWithTasksSorted()
        {
            return _dataStore.Projects
                .Select(project => project.Author)
                .Union(_dataStore.Projects
                    .SelectMany(project => project.Tasks)
                    .Select(task => task.Performer))
                .Distinct()
                .OrderBy(user => user.FirstName)
                .GroupJoin(_dataStore.Projects
                        .SelectMany(project => project.Tasks),
                    user => user.Id,
                    task => task.PerformerId,
                    (user, tasks) => new KeyValuePair<User, List<Task>>(user, tasks
                        .OrderByDescending(task => task.Name.Length)
                        .ToList()))
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        //6. Получить UserInfo (передать Id пользователя в параметры)
        // Последний проект пользователя (по дате создания)
        // Общее кол-во тасков под последним проектом
        // Общее кол-во незавершенных или отмененных тасков для пользователя
        // Самый долгий таск пользователя по дате (раньше всего создан - позже всего закончен)(фильтр по дате)
        public UserInfo GetUserInfo(int userId)
        {
            return _dataStore.Projects
                .Select(project => project.Author)
                .Union(_dataStore.Projects
                    .SelectMany(project => project.Tasks)
                    .Select(task => task.Performer))
                .Where(user => user.Id == userId)
                .Distinct() //users
                .GroupJoin(_dataStore.Projects,
                    user => user.Id,
                    project => project.AuthorId,
                    (user, projects) => new {User = user, Projects = projects}) //+ user own projects
                .GroupJoin(_dataStore.Projects
                        .SelectMany(project => project.Tasks),
                    userWithProjects => userWithProjects.User.Id,
                    task => task.PerformerId,
                    (userWithProjects, tasks) => //+ user tasks
                    {
                        var projectWithTasksCount = userWithProjects.Projects
                            .OrderByDescending(project => project.CreatedAt)
                            .Select(project => new {project, project.Tasks.ToList().Count})
                            .FirstOrDefault();
                        tasks = tasks.ToList();
                        return new UserInfo(projectWithTasksCount?.project,
                            projectWithTasksCount?.Count ?? 0,
                            tasks
                                .Count(task => task.FinishedAt == null || task.State == TaskState.Canceled),
                            tasks
                                .Where(task => task.FinishedAt != null)
                                .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                                .FirstOrDefault(),
                            userWithProjects.User
                        );
                    }
                ).FirstOrDefault();
        }

        public async Task<UserInfo> GetUserInfoWebApi(int userId)
        {
            return await ((UsersProvider) _provider).GetUserInfo(userId);
        }

        public async Task<List<UserTasks>> GetUsersSortedWithTasksSortedWebApi()
        {
            return await ((UsersProvider) _provider).GetUsersWithTasks();
        }
    }
}