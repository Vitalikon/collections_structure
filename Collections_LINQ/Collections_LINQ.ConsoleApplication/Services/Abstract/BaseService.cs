﻿using Collections_LINQ.ConsoleApplication.Providers.Abstract;

namespace Collections_LINQ.ConsoleApplication.Services.Abstract
{
    public abstract class BaseService<T>
    {
        private protected readonly DataStore _dataStore;
        private protected readonly IProvider<T> _provider;

        public BaseService(DataStore dataStore, IProvider<T> provider)
        {
            _dataStore = dataStore;
            _provider = provider;
        }
    }
}