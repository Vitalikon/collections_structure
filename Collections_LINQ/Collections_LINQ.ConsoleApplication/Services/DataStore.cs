﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Providers;
using ThreadingTask = System.Threading.Tasks.Task;

namespace Collections_LINQ.ConsoleApplication.Services
{
    public class DataStore
    {
        private readonly ProjectsProvider _projectsProvider;
        private readonly TasksProvider _tasksProvider;
        private readonly TeamsProvider _teamsProvider;
        private readonly UsersProvider _usersProvider;

        public List<Project> Projects;

        public DataStore()
        {
            _projectsProvider = new ProjectsProvider();
            _tasksProvider = new TasksProvider();
            _usersProvider = new UsersProvider();
            _teamsProvider = new TeamsProvider();
        }


        public async ThreadingTask BuildProjectsHierarchy()
        {
            try
            {
                var projectsTask = _projectsProvider.Get();
                var tasksTask = _tasksProvider.Get();
                var usersTask = _usersProvider.Get();
                var teamsTask = _teamsProvider.Get();
                ThreadingTask.WaitAll(projectsTask, tasksTask, usersTask, teamsTask);

                var projects = await projectsTask;
                var tasks = await tasksTask;
                var users = await usersTask;
                var teams = await teamsTask;

                Projects = projects
                    .GroupJoin(tasks,
                        p => p.Id,
                        t => t.ProjectId,
                        (project, tasksCollection) =>
                        {
                            project.Tasks = tasksCollection
                                .Join(users,
                                    task => task.PerformerId,
                                    user => user.Id,
                                    (resultTask, resultUser) =>
                                    {
                                        resultTask.Performer = resultUser;
                                        return resultTask;
                                    })
                                .ToList();
                            return project;
                        })
                    .Join(users,
                        p => p.AuthorId,
                        u => u.Id,
                        (project, user) =>
                        {
                            project.Author = user;
                            return project;
                        })
                    .Join(teams,
                        p => p.TeamId,
                        t => t.Id,
                        (project, team) =>
                        {
                            project.Team = team;
                            return project;
                        })
                    .ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("\nPress any button to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }
    }
}