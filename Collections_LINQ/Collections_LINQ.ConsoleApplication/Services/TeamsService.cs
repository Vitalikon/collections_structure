﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Collections_LINQ.ConsoleApplication.Services.Abstract;

namespace Collections_LINQ.ConsoleApplication.Services
{
    public class TeamsService : BaseService<Team>
    {
        public TeamsService(DataStore dataStore, IProvider<Team> provider) : base(dataStore, provider)
        {
        }

        public async Task<List<Team>> Get()
        {
            return await ((TeamsProvider) _provider).Get();
        }

        public async Task<Team> GetById(int id)
        {
            return await ((TeamsProvider) _provider).GetById(id);
        }

        public async Task<Team> Create(Team team)
        {
            return await ((TeamsProvider) _provider).Post(team);
        }

        public async Task<Team> Update(Team team)
        {
            return await ((TeamsProvider) _provider).Put(team);
        }

        public async void Delete(int id)
        {
            await ((TeamsProvider) _provider).Delete(id);
        }


        //4. Получить список (id, имя команды и список пользователей)
        //из коллекции команд, участники которых старше 10 лет,
        //отсортированных по дате регистрации пользователя по убыванию,
        //а также сгруппированных по командам.
        public List<(int Id, string Name, List<User> Users)> GetListOfTeamsOlder10SortedGrouped()
        {
            return _dataStore.Projects
                .Select(project => project.Team)
                .Distinct()
                .GroupJoin(_dataStore.Projects
                        .Select(project => project.Author)
                        .Union(_dataStore.Projects
                            .SelectMany(project => project.Tasks)
                            .Select(task => task.Performer))
                        .Distinct()
                        .Where(user => DateTime.Now.Year - user.BirthDay.Year > 10)
                        .OrderByDescending(user => user.RegisteredAt),
                    team => team.Id,
                    user => user.TeamId,
                    (team, users) => (team.Id, team.Name, users.ToList()))
                .ToList();
        }

        public async Task<List<Query4>> GetListOfTeamsOlder10SortedGroupedWebApi()
        {
            return await ((TeamsProvider) _provider).GetTeamsWithGroupedUsers();
        }
    }
}