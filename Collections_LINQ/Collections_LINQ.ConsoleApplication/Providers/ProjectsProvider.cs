﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Helpers;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace Collections_LINQ.ConsoleApplication.Providers
{
    public class ProjectsProvider : IProvider<Project>
    {
        private readonly HttpClient _httpClient;

        public ProjectsProvider()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(Settings.BaseApiUrl)
            };
        }

        public string ApiRoute => "/api/Projects";


        public async Task<List<Project>> Get()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<Project>>(content);
        }

        public async Task<Project> GetById(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Project>(content);
        }

        public async Task<Project> Post(Project entity)
        {
            var response = await _httpClient.PostAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Project>(content);
        }

        public async Task<Project> Put(Project entity)
        {
            var response = await _httpClient.PutAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Project>(content);
        }

        public async Task Delete(int id)
        {
            var response = await _httpClient.DeleteAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
        }

        public async Task<List<ProjectInfo>> GetProjectsInfo()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/getInfo");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<ProjectInfo>>(content);
        }
    }
}