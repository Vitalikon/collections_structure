﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Collections_LINQ.ConsoleApplication.Providers.Abstract
{
    public interface IProvider<T>
    {
        public string ApiRoute { get; }

        public Task<List<T>> Get();
        public Task<T> GetById(int id);
        public Task<T> Post(T entity);
        public Task<T> Put(T entity);
        public Task Delete(int id);
    }
}