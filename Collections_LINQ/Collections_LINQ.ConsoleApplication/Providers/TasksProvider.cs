﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Helpers;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Newtonsoft.Json;
using Task = Collections_LINQ.ConsoleApplication.Models.Entities.Task;

namespace Collections_LINQ.ConsoleApplication.Providers
{
    public class TasksProvider : IProvider<Task>
    {
        private readonly HttpClient _httpClient;

        public TasksProvider()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(Settings.BaseApiUrl)
            };
        }

        public string ApiRoute => "/api/Tasks";

        public async Task<List<Task>> Get()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<Task>>(content);
        }

        public async Task<Task> GetById(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Task>(content);
        }

        public async Task<Task> Post(Task entity)
        {
            var response = await _httpClient.PostAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Task>(content);
        }

        public async Task<Task> Put(Task entity)
        {
            var response = await _httpClient.PutAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Task>(content);
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var response = await _httpClient.DeleteAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
        }

        // 1
        public async Task<Dictionary<Project, int>> GetCountFromProjectsByAuthorId(int authorId)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/countFromProjectsByAuthorId/{authorId}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            var keyValue = JsonConvert.DeserializeObject<List<ProjectKeyIntValue>>(content);
            var result = new Dictionary<Project, int>();
            if (keyValue?.Count > 0) result = keyValue.ToDictionary(kv => kv.Key, kv => kv.Value);

            return result;
        }

        // 2
        public async Task<List<Task>> GetTasksByPerformerId(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/byPerformerId/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<Task>>(content);
        }

        // 3
        public async Task<List<Query3>> GetTasksFinishedByPerformerId(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/finishedByPerformerId/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<Query3>>(content);
        }


        private sealed class ProjectKeyIntValue
        {
            // добавил этот класс, потому что список из KeyValuePair не хотел парсится, а с классом все отлично работает
            public Project Key { get; set; }
            public int Value { get; set; }
        }
    }
}