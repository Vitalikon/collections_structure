﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Helpers;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace Collections_LINQ.ConsoleApplication.Providers
{
    public class UsersProvider : IProvider<User>
    {
        private readonly HttpClient _httpClient;

        public UsersProvider()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(Settings.BaseApiUrl)
            };
        }

        public string ApiRoute => "/api/Users";

        public async Task<List<User>> Get()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<User>>(content);
        }

        public async Task<User> GetById(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<User>(content);
        }

        public async Task<User> Post(User entity)
        {
            var response = await _httpClient.PostAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<User>(content);
        }

        public async Task<User> Put(User entity)
        {
            var response = await _httpClient.PutAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<User>(content);
        }

        public async Task Delete(int id)
        {
            var response = await _httpClient.DeleteAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
        }

        public async Task<UserInfo> GetUserInfo(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/{id}/getInfo");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<UserInfo>(content);
        }

        public async Task<List<UserTasks>> GetUsersWithTasks()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/withTasks");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<UserTasks>>(content);
        }
    }
}