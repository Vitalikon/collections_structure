﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Helpers;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Helpers;
using Collections_LINQ.ConsoleApplication.Providers.Abstract;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace Collections_LINQ.ConsoleApplication.Providers
{
    public class TeamsProvider : IProvider<Team>
    {
        private readonly HttpClient _httpClient;

        public TeamsProvider()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(Settings.BaseApiUrl)
            };
        }

        public string ApiRoute => "/api/Teams";

        public async Task<List<Team>> Get()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<Team>>(content);
        }

        public async Task<Team> GetById(int id)
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Team>(content);
        }

        public async Task<Team> Post(Team entity)
        {
            var response = await _httpClient.PostAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Team>(content);
        }

        public async Task<Team> Put(Team entity)
        {
            var response = await _httpClient.PutAsync($"{ApiRoute}",
                new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<Team>(content);
        }

        public async Task Delete(int id)
        {
            var response = await _httpClient.DeleteAsync($"{ApiRoute}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
        }

        public async Task<List<Query4>> GetTeamsWithGroupedUsers()
        {
            var response = await _httpClient.GetAsync($"{ApiRoute}/groupedUsers");
            var content = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                ExceptionThrowers.ThrowExceptionFromResponseBody(content);
            return JsonConvert.DeserializeObject<List<Query4>>(content);
        }
    }
}