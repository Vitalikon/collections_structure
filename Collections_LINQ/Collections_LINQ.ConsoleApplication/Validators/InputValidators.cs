﻿using System;
using System.IO;

namespace Collections_LINQ.ConsoleApplication.Validators
{
    public static class InputValidators
    {
        public static int IntegerInput()
        {
            var isSuccessParse = int.TryParse(Console.ReadLine(), out var input);
            if (!isSuccessParse)
                throw new IOException("Wrong input!");

            return input;
        }

        public static int IntegerInput(int minValue, int maxValue = int.MaxValue)
        {
            var input = IntegerInput();
            if (input < minValue || input > maxValue)
                throw new ArgumentOutOfRangeException("input", "Out of Range!");

            return input;
        }

        public static DateTime DateTimeInput(string fromString = null)
        {
            var isSuccessParse = DateTime.TryParse(fromString ?? Console.ReadLine(), out var input);
            if (!isSuccessParse)
                throw new IOException("Wrong input!");

            return input;
        }

        public static string StringInput()
        {
            var str = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(str))
                throw new IOException("Wrong input!");

            return str;
        }
    }
}