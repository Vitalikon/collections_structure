﻿using Collections_LINQ.ConsoleApplication.Models.Entities;

namespace Collections_LINQ.ConsoleApplication.Models.Helpers
{
    //7. Проект
    // Самый длинный таск проекта (по описанию)
    // Самый короткий таск проекта (по имени)
    // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3
    public class ProjectInfo
    {
        public ProjectInfo(Task longestTaskByDescription, Task shortestTaskByName, int countOfUsersInTeamsByCondition,
            Project project)
        {
            LongestTaskByDescription = longestTaskByDescription;
            ShortestTaskByName = shortestTaskByName;
            CountOfUsersInTeamsByCondition = countOfUsersInTeamsByCondition;
            Project = project;
        }

        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int CountOfUsersInTeamsByCondition { get; set; }

        public Project Project { get; set; }

        public override string ToString()
        {
            return $"Project: [\n{Project}\n]\n" +
                   $"Longest Task By Description: [\n{LongestTaskByDescription}\n]\n" +
                   $"Shortest Task By Name: [\n{ShortestTaskByName}\n]\n" +
                   $"Count of Users In Team where project description < 20 or tasks < 3: [{CountOfUsersInTeamsByCondition}]";
        }
    }
}