﻿using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Models.Entities;

namespace Collections_LINQ.ConsoleApplication.Models.Helpers
{
    public sealed class Query4
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}