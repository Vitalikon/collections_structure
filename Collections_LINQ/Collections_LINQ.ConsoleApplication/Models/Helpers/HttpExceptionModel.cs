﻿using System.Collections.Generic;
using System.Linq;

namespace Collections_LINQ.ConsoleApplication.Models.Helpers
{
    public class HttpExceptionModel
    {
        public string Title { get; set; }
        public int Status { get; set; }
        public Dictionary<string, List<string>> Errors { get; set; }

        public override string ToString()
        {
            var (key, value) = Errors.First();
            return $"\n{key}: {value.Aggregate((s1, s2) => s1 + "\n" + s2)}\n";
        }
    }
}