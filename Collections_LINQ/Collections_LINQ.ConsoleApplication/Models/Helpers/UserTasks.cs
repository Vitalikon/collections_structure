﻿using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Models.Entities;

namespace Collections_LINQ.ConsoleApplication.Models.Helpers
{
    public sealed class UserTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}