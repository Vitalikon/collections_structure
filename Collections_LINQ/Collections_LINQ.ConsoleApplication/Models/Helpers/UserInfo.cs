﻿using Collections_LINQ.ConsoleApplication.Models.Entities;

namespace Collections_LINQ.ConsoleApplication.Models.Helpers
{
    // User
    // Последний проект пользователя (по дате создания)
    // Общее кол-во тасков под последним проектом
    // Общее кол-во незавершенных или отмененных тасков для пользователя
    // Самый долгий таск пользователя по дате (раньше всего создан - позже всего закончен)(фильтр по дате)
    public class UserInfo
    {
        public UserInfo(
            Project lastProject,
            int lastProjectTasksCount,
            int countOfNotFinishedTasks,
            Task longestCompletedTaskByTime, User user)
        {
            LastProject = lastProject;
            LastProjectTasksCount = lastProjectTasksCount;
            CountOfNotFinishedTasks = countOfNotFinishedTasks;
            LongestCompletedTaskByTime = longestCompletedTaskByTime;
            User = user;
        }

        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int CountOfNotFinishedTasks { get; set; }
        public Task LongestCompletedTaskByTime { get; set; }

        public override string ToString()
        {
            return $"User: [\n{User}\n]\n" +
                   $"Last Project: [\n{LastProject}\n]\n" +
                   $"Last Project Tasks Count: [{LastProjectTasksCount}]\n" +
                   $"Count Of Not Finished Tasks: [{CountOfNotFinishedTasks}]\n" +
                   $"Longest Completed Task: [\n{LongestCompletedTaskByTime}\n]";
        }
    }
}