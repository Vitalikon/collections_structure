﻿namespace Collections_LINQ.ConsoleApplication.Models.Enums
{
    public enum TaskState
    {
        ToDo = 1,
        InProgress,
        Done,
        Canceled
    }
}