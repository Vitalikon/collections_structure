﻿using System;
using Collections_LINQ.ConsoleApplication.Models.Entities.Abstract;
using Collections_LINQ.ConsoleApplication.Models.Enums;

namespace Collections_LINQ.ConsoleApplication.Models.Entities
{
    public sealed class Task : BaseEntity
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"ProjectId: {ProjectId}\n" +
                   $"PerformerId: {PerformerId}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"State: {State}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"FinishedAt: {FinishedAt}";
        }
    }
}