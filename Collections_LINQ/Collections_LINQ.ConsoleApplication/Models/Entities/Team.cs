﻿using System;
using Collections_LINQ.ConsoleApplication.Models.Entities.Abstract;

namespace Collections_LINQ.ConsoleApplication.Models.Entities
{
    public sealed class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"CreatedAt: {CreatedAt}";
        }
    }
}