﻿namespace Collections_LINQ.ConsoleApplication.Models.Entities.Abstract
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}