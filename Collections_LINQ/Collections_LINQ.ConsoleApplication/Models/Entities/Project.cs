﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Models.Entities.Abstract;

namespace Collections_LINQ.ConsoleApplication.Models.Entities
{
    public sealed class Project : BaseEntity
    {
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public DateTime CreatedAt { get; set; }


        public ICollection<Task> Tasks { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"Deadline: {Deadline}\n" +
                   $"CreatedAt: {CreatedAt}";
        }
    }
}