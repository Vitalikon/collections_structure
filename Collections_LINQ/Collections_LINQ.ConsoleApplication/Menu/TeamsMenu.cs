﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Menu.Abstract;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Services;
using Collections_LINQ.ConsoleApplication.Validators;

namespace Collections_LINQ.ConsoleApplication.Menu
{
    public class TeamsMenu : IMenu
    {
        private readonly TeamsService _teamsService;

        public TeamsMenu(TeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        public List<string> MenuItems { get; } = new()
        {
            "1. GetAll",
            "2. Get by id",
            "3. Create",
            "4. Update",
            "5. Delete",
            "6. Back to MainMenu",
            "0. Exit"
        };

        public bool ShowMenu()
        {
            Console.WriteLine("Teams:");
            MenuItems.ForEach(Console.WriteLine);
            Console.Write("Select an option: ");
            var choice = InputValidators.IntegerInput(0, MenuItems.Count - 1);
            Console.Clear();

            return choice switch
            {
                1 => MenuGetTeams(),
                2 => MenuGetTeamById(),
                3 => MenuCreateNewTeam(),
                4 => MenuEditTeam(),
                5 => MenuDeleteTeam(),
                0 => false,
                _ => true
            };
        }

        private bool MenuDeleteTeam()
        {
            Console.WriteLine("Enter id of team to delete:");
            _teamsService.Delete(InputValidators.IntegerInput(0));
            return true;
        }

        private bool MenuEditTeam()
        {
            Console.WriteLine("Enter id of team to edit: ");
            var old = _teamsService.GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(old);
            Console.Write("\nEnter new Team name: ");
            old.Name = InputValidators.StringInput();
            var updated = _teamsService.Update(old).Result;
            Console.WriteLine($"\nCreated: \n{updated}");
            return true;
        }

        private bool MenuCreateNewTeam()
        {
            Console.Write("Enter Team name: ");
            var name = InputValidators.StringInput();
            var created = _teamsService.Create(new Team {Name = name}).Result;
            Console.WriteLine($"\nCreated: \n{created}");
            return true;
        }

        private bool MenuGetTeamById()
        {
            Console.Write("Enter team id: ");
            var team = _teamsService
                .GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(team);
            return true;
        }

        private bool MenuGetTeams()
        {
            var team = _teamsService.Get().Result;
            if (team.Count == 0)
            {
                Console.WriteLine("Not Found");
                return true;
            }

            foreach (var item in team)
                Console.WriteLine($"Project: [\n{item}\n]");
            return true;
        }
    }
}