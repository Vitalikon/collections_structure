﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Menu.Abstract;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Services;
using Collections_LINQ.ConsoleApplication.Validators;

namespace Collections_LINQ.ConsoleApplication.Menu
{
    public class UsersMenu : IMenu
    {
        private readonly UsersService _usersService;

        public UsersMenu(UsersService usersService)
        {
            _usersService = usersService;
        }

        public List<string> MenuItems { get; } = new()
        {
            "1. GetAll",
            "2. Get by id",
            "3. Create",
            "4. Update",
            "5. Delete",
            "6. Back to MainMenu",
            "0. Exit"
        };

        public bool ShowMenu()
        {
            Console.WriteLine("Users:");
            MenuItems.ForEach(Console.WriteLine);
            Console.Write("Select an option: ");
            var choice = InputValidators.IntegerInput(0, MenuItems.Count - 1);
            Console.Clear();

            return choice switch
            {
                1 => MenuGetUsers(),
                2 => MenuGetUserById(),
                3 => MenuCreateNewUser(),
                4 => MenuEditUser(),
                5 => MenuDeleteUser(),
                0 => false,
                _ => true
            };
        }

        private bool MenuDeleteUser()
        {
            Console.WriteLine("Enter id of user to delete:");
            _usersService.Delete(InputValidators.IntegerInput(0));
            return true;
        }

        private bool MenuEditUser()
        {
            Console.WriteLine("Enter User id:");
            var old = _usersService.GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(old);
            Console.WriteLine("\nEnter new values:");
            Console.WriteLine("Change user team id? y/n");
            if (InputValidators.StringInput().ToLower() == "y")
            {
                Console.WriteLine("TeamID:");
                old.TeamId = InputValidators.IntegerInput(0);
            }

            Console.WriteLine("FirstName:");
            old.FirstName = InputValidators.StringInput();
            Console.WriteLine("LastName:");
            old.LastName = InputValidators.StringInput();
            Console.WriteLine("Email:");
            old.Email = InputValidators.StringInput();
            Console.WriteLine("BirthDay:");
            Console.WriteLine("Enter in format like 2009-12-30 23:59:59 :");
            old.BirthDay = InputValidators.DateTimeInput();
            var updated = _usersService.Update(old).Result;
            Console.WriteLine($"\nUpdated: \n{updated}");
            return true;
        }

        private bool MenuCreateNewUser()
        {
            Console.WriteLine("Is User already in team? y/n");
            int? teamId;
            if (InputValidators.StringInput().ToLower() == "y")
            {
                Console.WriteLine("TeamID:");
                teamId = InputValidators.IntegerInput(0);
            }
            else
            {
                teamId = null;
            }

            Console.WriteLine("FirstName:");
            var firstName = InputValidators.StringInput();
            Console.WriteLine("LastName:");
            var lastName = InputValidators.StringInput();
            Console.WriteLine("Email:");
            var email = InputValidators.StringInput();
            Console.WriteLine("BirthDay:");
            Console.WriteLine("Enter in format like 2009-12-30 23:59:59:");
            var birthDay = InputValidators.DateTimeInput();

            var created = _usersService.Create(new User
            {
                Email = email,
                BirthDay = birthDay,
                FirstName = firstName,
                LastName = lastName,
                TeamId = teamId
            }).Result;
            Console.WriteLine($"\nCreated: \n{created}");
            return true;
        }

        private bool MenuGetUserById()
        {
            Console.Write("Enter user id: ");
            var user = _usersService
                .GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(user);
            return true;
        }

        private bool MenuGetUsers()
        {
            var users = _usersService.Get().Result;
            if (users.Count == 0)
            {
                Console.WriteLine("Not Found");
                return true;
            }

            foreach (var item in users)
                Console.WriteLine($"User: [\n{item}\n]");
            return true;
        }
    }
}