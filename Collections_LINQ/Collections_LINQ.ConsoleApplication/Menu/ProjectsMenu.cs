﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Menu.Abstract;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Services;
using Collections_LINQ.ConsoleApplication.Validators;

namespace Collections_LINQ.ConsoleApplication.Menu
{
    public class ProjectsMenu : IMenu
    {
        private readonly ProjectsService _projectsService;

        public ProjectsMenu(ProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        public List<string> MenuItems { get; } = new()
        {
            "1. GetAll",
            "2. Get by id",
            "3. Create",
            "4. Update",
            "5. Delete",
            "6. Back to MainMenu",
            "0. Exit"
        };

        public bool ShowMenu()
        {
            Console.WriteLine("Projects:");
            MenuItems.ForEach(Console.WriteLine);
            Console.Write("Select an option: ");
            var choice = InputValidators.IntegerInput(0, MenuItems.Count - 1);
            Console.Clear();

            return choice switch
            {
                1 => MenuGetProjects(),
                2 => MenuGetProjectById(),
                3 => MenuCreateNewProject(),
                4 => MenuEditProject(),
                5 => MenuDeleteProject(),
                0 => false,
                _ => true
            };
        }

        public bool MenuGetProjects()
        {
            var projects = _projectsService.Get().Result;
            if (projects.Count == 0)
            {
                Console.WriteLine("Not Found");
                return true;
            }

            foreach (var item in projects)
                Console.WriteLine($"Project: [\n{item}\n]");
            return true;
        }

        public bool MenuGetProjectById()
        {
            Console.Write("Enter project id: ");
            var project = _projectsService
                .GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(project);
            return true;
        }

        public bool MenuCreateNewProject()
        {
            Console.WriteLine("Enter the values in turn:");
            Console.WriteLine("AuthorId:\n" +
                              "TeamId:\n" +
                              "Name:\n" +
                              "Description:\n" +
                              "Deadline:");
            var authorId = InputValidators.IntegerInput(0);
            var teamId = InputValidators.IntegerInput(0);
            var name = InputValidators.StringInput();
            var description = InputValidators.StringInput();
            var deadline = InputValidators.DateTimeInput();
            if (deadline < DateTime.Now) Console.WriteLine("Deadline must be in future");

            var created = _projectsService.Create(new Project
            {
                TeamId = teamId,
                AuthorId = authorId,
                Name = name,
                Description = description,
                Deadline = deadline
            }).Result;
            Console.WriteLine($"\nCreated: \n{created}");
            return true;
        }

        public bool MenuEditProject()
        {
            Console.WriteLine("Enter id of project to edit: ");
            var oldProject = _projectsService.GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(oldProject);
            Console.WriteLine("Enter new values in turn:");
            Console.WriteLine("AuthorId:\n" +
                              "TeamId:\n" +
                              "Name:\n" +
                              "Description:\n" +
                              "Deadline:\n");
            oldProject.AuthorId = InputValidators.IntegerInput(0);
            oldProject.TeamId = InputValidators.IntegerInput(0);
            oldProject.Name = InputValidators.StringInput();
            oldProject.Description = InputValidators.StringInput();
            oldProject.Deadline = InputValidators.DateTimeInput();
            var updated = _projectsService.Update(oldProject).Result;
            Console.WriteLine($"\nUpdated project: \n{updated}");
            return true;
        }

        public bool MenuDeleteProject()
        {
            Console.WriteLine("Enter id of project to delete:");
            _projectsService.Delete(InputValidators.IntegerInput(0));
            return true;
        }
    }
}