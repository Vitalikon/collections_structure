﻿using System.Collections.Generic;

namespace Collections_LINQ.ConsoleApplication.Menu.Abstract
{
    public interface IMenu
    {
        public List<string> MenuItems { get; }
        public bool ShowMenu();
    }
}