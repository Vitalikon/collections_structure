﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Menu.Abstract;
using Collections_LINQ.ConsoleApplication.Models.Entities;
using Collections_LINQ.ConsoleApplication.Models.Enums;
using Collections_LINQ.ConsoleApplication.Services;
using Collections_LINQ.ConsoleApplication.Validators;
using static System.Enum;

namespace Collections_LINQ.ConsoleApplication.Menu
{
    public class TasksMenu : IMenu
    {
        private readonly TasksService _tasksService;

        public TasksMenu(TasksService tasksService)
        {
            _tasksService = tasksService;
        }

        public List<string> MenuItems { get; } = new()
        {
            "1. GetAll",
            "2. Get by id",
            "3. Create",
            "4. Update",
            "5. Delete",
            "6. Back to MainMenu",
            "0. Exit"
        };

        public bool ShowMenu()
        {
            Console.WriteLine("Tasks:");
            MenuItems.ForEach(Console.WriteLine);
            Console.Write("Select an option: ");
            var choice = InputValidators.IntegerInput(0, MenuItems.Count - 1);
            Console.Clear();

            return choice switch
            {
                1 => MenuGetTasks(),
                2 => MenuGetTaskById(),
                3 => MenuCreateNewTask(),
                4 => MenuEditTask(),
                5 => MenuDeleteTask(),
                0 => false,
                _ => true
            };
        }

        private bool MenuDeleteTask()
        {
            Console.WriteLine("Enter id of task to delete:");
            _tasksService.Delete(InputValidators.IntegerInput(0));
            return true;
        }

        private bool MenuEditTask()
        {
            Console.WriteLine("Enter id of task to edit: ");
            var oldTask = _tasksService.GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(oldTask);
            Console.WriteLine("\nEnter the values in turn:");
            Console.WriteLine("\nProjectId:\n" +
                              "PerformerId:\n" +
                              "Name:\n" +
                              "Description:\n" +
                              "State:\n" +
                              "FinishedAt:");
            oldTask.ProjectId = InputValidators.IntegerInput(0);
            oldTask.PerformerId = InputValidators.IntegerInput(0);
            oldTask.Name = InputValidators.StringInput();
            oldTask.Description = InputValidators.StringInput();
            Console.WriteLine($"\n1. {TaskState.ToDo}, " +
                              $"2. {TaskState.InProgress}, " +
                              $"3. {TaskState.Done}, " +
                              $"4. {TaskState.Canceled}\n" +
                              "Select number of enum:");
            if (!TryParse<TaskState>(InputValidators.IntegerInput(1, 4).ToString(), out var state))
            {
                Console.WriteLine("error");
                return true;
            }

            oldTask.State = state;

            Console.WriteLine("Finished y/n");
            var isFinishedString = InputValidators.StringInput();
            if (isFinishedString.ToLower() == "y")
            {
                Console.WriteLine("Enter in format like 2009-12-30 23:59:59:");
                oldTask.FinishedAt = InputValidators.DateTimeInput();
            }

            var updated = _tasksService.Update(oldTask).Result;
            Console.WriteLine($"\nUpdated: \n{updated}");
            return true;
        }

        private bool MenuCreateNewTask()
        {
            Console.WriteLine("Enter the values in turn:");
            Console.WriteLine("ProjectId:\n" +
                              "PerformerId:\n" +
                              "Name:\n" +
                              "Description:\n" +
                              "State:");
            var projectId = InputValidators.IntegerInput(0);
            var performerId = InputValidators.IntegerInput(0);
            var name = InputValidators.StringInput();
            var description = InputValidators.StringInput();
            Console.WriteLine($"\n1. {TaskState.ToDo}, " +
                              $"2. {TaskState.InProgress}, " +
                              $"3. {TaskState.Done}, " +
                              $"4. {TaskState.Canceled}\n" +
                              "Select number of enum:");
            if (!TryParse<TaskState>(InputValidators.IntegerInput(1, 4).ToString(), out var state))
            {
                Console.WriteLine("error");
                return true;
            }

            var created = _tasksService.Create(new Task
            {
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description,
                State = state
            }).Result;
            Console.WriteLine($"\nCreated: \n{created}");
            return true;
        }

        private bool MenuGetTaskById()
        {
            Console.Write("Enter task id: ");
            var task = _tasksService
                .GetById(InputValidators.IntegerInput(0)).Result;
            Console.WriteLine(task);
            return true;
        }

        private bool MenuGetTasks()
        {
            var tasks = _tasksService.Get().Result;
            if (tasks.Count == 0)
            {
                Console.WriteLine("Not Found");
                return true;
            }

            foreach (var item in tasks)
                Console.WriteLine($"Task: [\n{item}\n]");
            return true;
        }
    }
}