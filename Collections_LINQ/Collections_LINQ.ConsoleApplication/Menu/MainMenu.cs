﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Menu.Abstract;
using Collections_LINQ.ConsoleApplication.Services;
using Collections_LINQ.ConsoleApplication.Validators;

namespace Collections_LINQ.ConsoleApplication.Menu
{
    public class MainMenu : IMenu
    {
        private readonly MenuLocalDataQueries _menuLocalDataQueries;
        private readonly MenuWebApiDataQueries _menuWebApiDataQueries;
        private readonly ProjectsMenu _projectsMenu;

        private readonly ProjectsService _projectsService;
        private readonly TasksMenu _tasksMenu;
        private readonly TasksService _tasksService;
        private readonly TeamsMenu _teamsMenu;
        private readonly TeamsService _teamsService;
        private readonly UsersMenu _usersMenu;
        private readonly UsersService _usersService;

        public MainMenu(ProjectsService projectsService, TasksService tasksService, TeamsService teamsService,
            UsersService usersService)
        {
            _projectsService = projectsService;
            _tasksService = tasksService;
            _teamsService = teamsService;
            _usersService = usersService;
            _menuLocalDataQueries =
                new MenuLocalDataQueries(projectsService, tasksService, teamsService, usersService);
            _menuWebApiDataQueries =
                new MenuWebApiDataQueries(projectsService, tasksService, teamsService, usersService);
            _usersMenu = new UsersMenu(usersService);
            _projectsMenu = new ProjectsMenu(projectsService);
            _tasksMenu = new TasksMenu(tasksService);
            _teamsMenu = new TeamsMenu(teamsService);
        }

        public List<string> MenuItems { get; } = new()
        {
            "1. Old Menu with Local data queries",
            "2. New Menu with WebAPI data queries",
            "3. Projects menu",
            "4. Users menu",
            "5. Tasks menu",
            "6. Teams menu",
            "0. Exit"
        };

        public bool ShowMenu()
        {
            MenuItems.ForEach(Console.WriteLine);
            Console.Write("Select an option: ");
            var choice = InputValidators.IntegerInput(0, MenuItems.Count - 1);
            Console.Clear();

            return choice switch
            {
                1 => _menuLocalDataQueries.ShowMenu(),
                2 => _menuWebApiDataQueries.ShowMenu(),
                3 => _projectsMenu.ShowMenu(),
                4 => _usersMenu.ShowMenu(),
                5 => _tasksMenu.ShowMenu(),
                6 => _teamsMenu.ShowMenu(),
                0 => false,
                _ => true
            };
        }
    }
}