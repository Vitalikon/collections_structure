﻿using System;
using System.Collections.Generic;
using Collections_LINQ.ConsoleApplication.Menu.Abstract;
using Collections_LINQ.ConsoleApplication.Services;
using Collections_LINQ.ConsoleApplication.Validators;

namespace Collections_LINQ.ConsoleApplication.Menu
{
    public class MenuLocalDataQueries : IMenu
    {
        private readonly ProjectsService _projectsService;
        private readonly TasksService _tasksService;
        private readonly TeamsService _teamsService;
        private readonly UsersService _usersService;

        public MenuLocalDataQueries(ProjectsService projectsService, TasksService tasksService,
            TeamsService teamsService,
            UsersService usersService)
        {
            _projectsService = projectsService;
            _tasksService = tasksService;
            _teamsService = teamsService;
            _usersService = usersService;
        }

        public List<string> MenuItems { get; } = new()
        {
            "1. Get count of tasks from projects by user id",
            "2. Get tasks by performer id (task.name < 45)",
            "3. Get list of finished tasks by performer id (Id, Name)",
            "4. Get list of teams where users older than 10 sorted by descending by registrationDate and grouped by teams",
            "5. Get users sorted by ascending with tasks sorted by descending",
            "6. Get user info by id",
            "7. Get projects info",
            "8. Back to Main Menu",
            "0. Exit"
        };

        public bool ShowMenu()
        {
            MenuItems.ForEach(Console.WriteLine);
            Console.Write("Select an option: ");
            var choice = InputValidators.IntegerInput(0, MenuItems.Count - 1);
            Console.Clear();

            switch (choice)
            {
                case 1:
                    MenuGetCountOfTasksFromProjectsByAuthorId();
                    return true;
                case 2:
                    MenuGetTasksByPerformerId();
                    return true;
                case 3:
                    MenuGetListOfFinishedTasksByPerformerId();
                    return true;
                case 4:
                    MenuListOfTeamsOlder10SortedGrouped();
                    return true;
                case 5:
                    MenuGetUsersSortedByAscendingWithTasksSortedByDescending();
                    return true;
                case 6:
                    MenuGetUserInfo();
                    return true;
                case 7:
                    MenuGetProjectsInfo();
                    return true;
                case 0:
                    return false;
                default:
                    return true;
            }
        }


        // #1
        public void MenuGetCountOfTasksFromProjectsByAuthorId()
        {
            Console.Write("Enter author id: ");
            var dictionary = _tasksService
                .GetCountOfTasksFromProjectsByAuthorId(InputValidators.IntegerInput(0));
            if (dictionary.Count == 0)
            {
                Console.WriteLine("Not Found");
                return;
            }

            foreach (var (key, value) in dictionary)
                Console.WriteLine($"Project Name: [{key.Name}], count of tasks: [{value}]");
        }

        // #2
        public void MenuGetTasksByPerformerId()
        {
            Console.Write("Enter performer id: ");
            var tasks = _tasksService
                .GetTasksByPerformerId(InputValidators.IntegerInput(0));
            if (tasks.Count == 0)
            {
                Console.WriteLine("Not Found");
                return;
            }

            foreach (var item in tasks)
                Console.WriteLine($"Task Name: [{item.Name}]");
        }

        // #3
        public void MenuGetListOfFinishedTasksByPerformerId()
        {
            Console.Write("Enter performer id: ");
            var tasks = _tasksService
                .GetListOfFinishedTasksByPerformerId(InputValidators.IntegerInput(0));
            if (tasks.Count == 0)
            {
                Console.WriteLine("Not Found");
                return;
            }

            foreach (var (key, value) in tasks)
                Console.WriteLine($"Task Id: [{key}], Task Name: [{value}]");
        }

        // #4
        public void MenuListOfTeamsOlder10SortedGrouped()
        {
            var teams = _teamsService
                .GetListOfTeamsOlder10SortedGrouped();
            if (teams.Count == 0)
            {
                Console.WriteLine("Not Found");
                return;
            }

            foreach (var (id, name, users) in teams)
                Console.WriteLine($"TeamId: [{id}], TeamName: [{name}], Users count: {users.Count}");
        }

        // #5
        public void MenuGetUsersSortedByAscendingWithTasksSortedByDescending()
        {
            var usersTasks = _usersService.GetUsersSortedWithTasksSorted();
            if (usersTasks.Count == 0)
            {
                Console.WriteLine("Not Found");
                return;
            }

            foreach (var (user, tasks) in usersTasks)
            {
                Console.WriteLine($"User FirstName: [{user.FirstName}], Tasks: [");
                tasks.ForEach(task => Console.WriteLine($"-   {task.Name}"));
                Console.WriteLine("]");
            }
        }

        // #6
        public void MenuGetUserInfo()
        {
            Console.Write("Enter user id: ");
            var userInfo = _usersService.GetUserInfo(InputValidators.IntegerInput(0));
            if (userInfo == null)
            {
                Console.WriteLine("Not Found");
                return;
            }

            Console.WriteLine(userInfo);
        }

        // #7
        public void MenuGetProjectsInfo()
        {
            var projectsInfo = _projectsService.GetProjectsInfo();
            if (projectsInfo.Count == 0)
            {
                Console.WriteLine("Not Found");
                return;
            }

            projectsInfo.ForEach(info => Console.WriteLine($"{info}\n"));
        }
    }
}