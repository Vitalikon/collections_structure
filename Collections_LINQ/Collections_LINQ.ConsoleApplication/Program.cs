﻿using System;
using System.Threading.Tasks;
using Collections_LINQ.ConsoleApplication.Menu;
using Collections_LINQ.ConsoleApplication.Providers;
using Collections_LINQ.ConsoleApplication.Services;

namespace Collections_LINQ.ConsoleApplication
{
    internal static class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("Loading...");
            var dataStore = new DataStore();
            await dataStore.BuildProjectsHierarchy();
            var menu = new MainMenu(
                new ProjectsService(dataStore, new ProjectsProvider()),
                new TasksService(dataStore, new TasksProvider()),
                new TeamsService(dataStore, new TeamsProvider()),
                new UsersService(dataStore, new UsersProvider()));
            var showMenu = true;
            while (showMenu)
            {
                try
                {
                    Console.Clear();
                    showMenu = menu.ShowMenu();
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    if (!e.Message.Contains("Async"))
                        Console.WriteLine("Error: " + e.Message);
                    if (e.InnerException != null)
                        if (!e.Message.Contains(e.InnerException.Message) &&
                            !e.InnerException.Message.Contains("Async"))
                            Console.WriteLine($"{"Inner Error:" + e.InnerException.Message}");
                    Console.ResetColor();
                }

                Console.WriteLine("\nPress any button to continue...");
                Console.ReadKey();
            }
        }
    }
}